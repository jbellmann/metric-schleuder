package de.jbellmann.metricschleuder

import mu.KotlinLogging
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
class ScheduledHttpClient {

    private var localPort: Int = 8080 // take it as default

    private val rest = RestTemplate()
    private val log = KotlinLogging.logger {}

    @Scheduled(initialDelay = 10_000, fixedDelay = 20_000)
    fun createTraffic() {
        log.info { "Generate traffic to localhost:$localPort" }
        (0..10).forEach {
            rest.getForEntity("http://localhost:$localPort/ping", String::class.java)
        }
        (0..1).forEach {
            rest.getForEntity("http://localhost:$localPort/long-ping", String::class.java)
        }
        log.info { "... traffic generation done" }
    }

    @EventListener
    fun onApplicationReady(event: ApplicationReadyEvent) {
        this.localPort = event.applicationContext.environment.getProperty("local.server.port", Int::class.java, 8080)
        log.info { "LocalPort was set to:  ${this.localPort}" }
    }
}