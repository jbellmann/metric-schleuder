package de.jbellmann.metricschleuder

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.lang.Thread.sleep

@RestController
class SimpleController {

    @GetMapping("/ping")
    fun ping(): ResponseEntity<String> {
        return ResponseEntity.ok("PONG")
    }

    @GetMapping("/long-ping")
    fun longPing(): ResponseEntity<String> {
        sleep(4_000)
        return ResponseEntity.ok("LONG_PONG")
    }
}