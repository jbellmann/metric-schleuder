package de.jbellmann.metricschleuder

import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling

@EnableScheduling
@Configuration
class SchedulingConfig {
}