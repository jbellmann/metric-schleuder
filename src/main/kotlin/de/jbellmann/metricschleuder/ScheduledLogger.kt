package de.jbellmann.metricschleuder

import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class ScheduledLogger {

    val log = KotlinLogging.logger {}

    @Scheduled(initialDelay = 5_000, fixedDelay = 10_000)
    fun printLogs() {
        log.trace { "LOG some trace messages ...." }
        log.debug { "LOG some debug messages ...." }
        log.info { "LOG some info messages ...." }
        log.warn { "LOG some warn messages ...." }
        log.error { "LOG some error messages ...." }
    }
}