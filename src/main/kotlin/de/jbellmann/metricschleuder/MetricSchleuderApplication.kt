package de.jbellmann.metricschleuder

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MetricSchleuderApplication

fun main(args: Array<String>) {
    runApplication<MetricSchleuderApplication>(*args)
}
